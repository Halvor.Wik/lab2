package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    //field variables
    int max_size = 20;
    ArrayList<FridgeItem> fridgeItems;

    //Constructor
    public Fridge() {
        fridgeItems = new ArrayList<>();
    }

    //Methods
    @Override
    public int nItemsInFridge() {
        return fridgeItems.size();
    }

    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < max_size) {
            return fridgeItems.add(item);
        }
        else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridgeItems.contains(item)) {
            fridgeItems.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }

    }

    @Override
    public void emptyFridge() {
        fridgeItems.clear();

    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFridgeItems = new ArrayList<>();

        for (FridgeItem i: fridgeItems) {
            if (i.hasExpired()) {;
                expiredFridgeItems.add(i);
            }
        }
        fridgeItems.removeAll(expiredFridgeItems);
        return expiredFridgeItems;
    }
}
